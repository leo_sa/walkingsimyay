﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class theEvent : MonoBehaviour
{
    public bool random, once;

    public GameObject[] toSpawn;

    bool actualOnce = true;

    Transform pointToInstantiate;

    void Start()
    {
        pointToInstantiate = transform.GetChild(0);

    }

    private void OnTriggerEnter(Collider other)
    {
        if (once && actualOnce && other.gameObject.tag =="Player")
        {
            for (int i = 0; i < toSpawn.Length; i++)
            {
                Instantiate(toSpawn[i], pointToInstantiate.position, Quaternion.identity);
            }
            actualOnce = false;
        }
        else if (!once)
        {
            for (int i = 0; i < toSpawn.Length; i++)
            {
                Instantiate(toSpawn[i], pointToInstantiate.position, Quaternion.identity);
            }
        }
    }
}
