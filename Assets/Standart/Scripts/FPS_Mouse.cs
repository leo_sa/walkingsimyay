﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS_Mouse : MonoBehaviour
{
    public float sensitivity = 100f;

    public Transform playBod;

    float xRotation = 0f;

    float mx, my;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        if (playBod == null) playBod = transform.parent;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) Application.Quit();

        if (!Input.GetMouseButton(0))
        {
            mx = Input.GetAxis("Mouse X") * sensitivity * Time.deltaTime;
            my = Input.GetAxis("Mouse Y") * sensitivity * Time.deltaTime;           
        }
        else
        {
            mx = 0;
            my = 0;
        }
      

        xRotation -= my;
        xRotation = Mathf.Clamp(xRotation, -90f, 90f);      

        transform.localRotation = Quaternion.Euler(xRotation, 0f, transform.eulerAngles.z);
        playBod.Rotate(Vector3.up * mx);
    }
}
