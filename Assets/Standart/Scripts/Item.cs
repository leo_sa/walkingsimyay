﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Item : MonoBehaviour
{
    Transform itemPos, cam;

    public float weight = 2;

    Rigidbody rb;

    [HideInInspector]
    public bool isChosen, doIt, grounded;

    float mx, my;


    void Start()
    {
        rb = GetComponent<Rigidbody>();
        cam = Camera.main.transform;
        itemPos = cam.GetChild(0).transform;
    }

    
    void Update()
    {
        if (isChosen)
        {
            transform.position = itemPos.position;

            if (Input.GetMouseButton(0))
            {
                mx = Input.GetAxis("Mouse X") * 100 * Time.deltaTime;
                my = Input.GetAxis("Mouse Y") * 100 * Time.deltaTime;
                transform.rotation = Quaternion.Euler(0, transform.eulerAngles.y + mx, transform.eulerAngles.z + my);

            }
            else if (Input.GetMouseButtonUp(0))
            {
                mx = 0;
                my = 0;
            }
            else transform.rotation = Quaternion.Lerp(transform.rotation, cam.rotation, 1 - Mathf.Clamp(weight,0.1f, 99)/10);

            if (Input.GetKeyDown("q")) doIt = true;
            else doIt = false;
        }
        else rb.AddForce(Vector3.up * -weight);
    }


    private void OnCollisionEnter(Collision collision)
    {
        isChosen = false;
        grounded = true;
    }


    private void OnCollisionExit(Collision collision)
    {
        grounded = false;
    }
}
