﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Alice_Mechanics : MonoBehaviour
{
    [HideInInspector]
    public bool schrumpf, normalisier, wachs;

    public float mini = 0.2f, groß = 2;

    [Range(0, 1)]
    public float changeSpeed = 0.2f;
   
    void Update()
    {
        if (schrumpf) smoler();
        if (normalisier) normalize();
        if (wachs) larger();
    }

    public void smoler()
    {
        if (transform.localScale.x > mini) transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(mini, mini, mini), changeSpeed);
        if (Mathf.Abs(transform.localScale.x - mini) <= 0.01f) schrumpf = false;           
    }

    public void normalize()
    {
        if (transform.localScale.x != 1) transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(1, 1, 1), changeSpeed);
        if (Mathf.Abs(transform.localScale.x - 1) <= 0.01f) normalisier = false;
    }

    public void larger()
    {
        if (transform.localScale.x < groß) transform.localScale = Vector3.Lerp(transform.localScale, new Vector3(groß, groß, groß), changeSpeed);
        if (Mathf.Abs(transform.localScale.x - groß) <= 0.01f) wachs = false;
    }
}
