﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pickUp : MonoBehaviour
{
    Transform cam, itemPos;

    public LayerMask lay;

    RaycastHit hit;

    float mx, my;

    void Start()
    {
        cam = Camera.main.transform;
        itemPos = cam.GetChild(0).transform;
    }

   
    void Update()
    {
        if(Physics.Raycast(cam.position, cam.forward, out hit, 3, lay))
        {
            if(hit.collider.gameObject.tag == "PickupAble")
            {
                if (Input.GetKey("e"))
                {
                    hit.collider.gameObject.GetComponent<Item>().isChosen = true;
                }
                else hit.collider.gameObject.GetComponent<Item>().isChosen = false;
            }
        }
        
    }
}
