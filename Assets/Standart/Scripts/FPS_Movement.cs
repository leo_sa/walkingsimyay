﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPS_Movement : MonoBehaviour2
{
    public float maxSpeed = 14, jumpHeight = 10, gravity = 2;

    public int maxJumps = 2;

    [Range(0, 1)]
    public float SpeedAcceleration = 0.9f, baseDirAcceleration = 0.3f, zoomSpeed = 0.1f;

    [Range(0, 2)]
    public float zoomMultiplicationFactor = 0.5f;

    Rigidbody rb;

    Camera cam;

    GameObject camera;

    float x, y, z;                       //directions and gravity

    float speedX, speedY;

    int jumpAmounts, ogFov;

    bool grounded, fovBacc, zoomBacc, canClimb;

    Vector3 ogPos, moveTo;

    //Animator camAnim;

    public LayerMask lay;

    RaycastHit hit;


    void Start()
    {
        ogPos = transform.position;
        rb = GetComponent<Rigidbody>();
        cam = Camera.main;
        camera = Camera.main.gameObject;
        ogFov = (int)cam.fieldOfView;
        //camAnim = camera.GetComponent<Animator>();
    }


    void Update()
    {      
        rb.AddForce(Vector3.up * -gravity);                  //Applying Gravity

        if (transform.position.y <= -10) backToStart();
        
            move();
            jump();    
            zoom();
            climb();
    }
    

    void move()
    {
        x = Mathf.Lerp(x, Input.GetAxisRaw("Horizontal"), baseDirAcceleration);
        y = Mathf.Lerp(y, Input.GetAxisRaw("Vertical"), baseDirAcceleration);

        //if (x != 0 || y != 0) camAnim.SetBool("iswalking", true);
        //else camAnim.SetBool("iswalking", false);

        speedX = Mathf.Lerp(x * x * maxSpeed, speedX, x * x * SpeedAcceleration);
        speedY = Mathf.Lerp(y * y * maxSpeed, speedY, y * y * SpeedAcceleration);

        if(!canClimb) moveTo = transform.right * x * speedX + transform.forward * y * speedY + Vector3.up * rb.velocity.y;
        else moveTo = transform.right * x * speedX + Vector3.up * y * speedY;

        rb.velocity = moveTo;

        if (Physics.Raycast(transform.position, transform.right, 1 * transform.localScale.x))
        {
            speedX = Mathf.Clamp(speedX, 0, maxSpeed);
            x = Mathf.Clamp(x, -1, 0);
        }
        else if (Physics.Raycast(transform.position, -transform.right, 1 * transform.localScale.x))
        {
            speedX = Mathf.Clamp(speedX, 0, maxSpeed);
            x = Mathf.Clamp(x, 0, 1);
        }

    }

    void jump()
    {
        if (Input.GetKeyDown(KeyCode.Space) && jumpAmounts > 0)
        {
            rb.AddForce(Vector3.up * jumpHeight, ForceMode.Impulse);           
            jumpAmounts--;
        }
    }


    void backToStart()
    {
        while (Vector3.Distance(transform.position, ogPos) >= 1) transform.position = Vector3.Lerp(transform.position, ogPos, zoomSpeed);
    }

    void zoom()
    {

        if (Input.GetMouseButton(1))
        {
            Time.timeScale = 0.2f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;


            if (cam.fieldOfView != ogFov * 0.2f)
            {
                cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, ogFov * zoomMultiplicationFactor, zoomSpeed);
            }
        }
        else if (Input.GetMouseButtonUp(1))
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = 0.02f * Time.timeScale;

            zoomBacc = true;
        }
        else if (zoomBacc && cam.fieldOfView != ogFov) cam.fieldOfView = Mathf.Lerp(cam.fieldOfView, ogFov, 0.1f);
        else if (zoomBacc && cam.fieldOfView == ogFov) zoomBacc = false;

    }

    void climb()
    {
       //if(Physics.SphereCast(new Vector3(transform.position.x, transform.position.y - 0.4f, transform.position.z), transform.localScale.x, Vector3.up)
       if(Physics.SphereCast(transform.position, 1 * transform.localScale.x, Vector3.up, out hit, lay))
        {
            Log("is");
        }

    }


    private void OnCollisionEnter(Collision collision)
    {
        grounded = true;
        jumpAmounts = maxJumps;

        if (Input.GetKey(KeyCode.Space) && jumpAmounts > 0)
        {
            rb.AddForce(Vector3.up * jumpHeight, ForceMode.Impulse);                        //AQ(action Queue) Jump           
            jumpAmounts--;
        }

        if (collision.gameObject.tag == "ClimbAble") canClimb = true;
    }

    private void OnCollisionExit(Collision collision)
    {
        grounded = false;
        if (collision.gameObject.tag == "ClimbAble") canClimb = false;
    }

}
