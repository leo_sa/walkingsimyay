﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonoBehaviour2 : MonoBehaviour
{
    public void Log(object tolog)
    {
        Debug.Log(tolog);
    }


    public void MyPlay(AudioSource ass, float range)
    {
        if (range == 0) range = 0.05f;
        ass.Stop();
        ass.pitch = Random.Range(1 - range, 1 + range);
        ass.Play();
    }

    public void ControlAccelerate(float num, float accelerationRate, float max, float timer)
    {
        while(timer >= 0)
        {
            num += Time.deltaTime * accelerationRate;
            timer -= Time.deltaTime;

            if (num >= max) num = max;
        }
    }

}
