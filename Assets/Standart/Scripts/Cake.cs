﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cake : MonoBehaviour
{
    Item item;

    public bool schrumpf, normalisier, wachs;

    public float mini = 0.2f, groß = 2;

    [Range(0, 1)]
    public float changeSpeed = 0.2f;

    GameObject player;

    int n;

    private void Start()
    {
        item = GetComponent<Item>();
        player = GameObject.Find("Player");
        if (schrumpf) n = 1;
        if (normalisier) n = 2;
        if (wachs) n = 3;
        if (!schrumpf && !normalisier && !wachs) n = Random.Range(1, 3);
    }

    private void Update()
    {
        if (item.doIt)
        {
            if (n == 1) smoler();
            if (n == 2) normalize();
            if (n == 3) larger();         
        }      
    }

    public void smoler()
    {
        while (player.transform.localScale.x > mini)
        {
            player.transform.localScale = new Vector3(player.transform.localScale.x - changeSpeed, player.transform.localScale.y - changeSpeed, player.transform.localScale.z - changeSpeed);
            if (Mathf.Abs(transform.localScale.x - mini) <= 0.1f) schrumpf = false;
        }
       
    }

    public void normalize()
    {
        while (Mathf.Abs(transform.localScale.x - 1) <= 0.1f)
        {
            player.transform.localScale = Vector3.Lerp(player.transform.localScale, new Vector3(1, 1, 1), changeSpeed);
            
        }
        if (player.transform.localScale.x != 1) normalisier = false;


    }

    public void larger()
    {
        while (player.transform.localScale.x < groß)
        {           
            player.transform.localScale = new Vector3(player.transform.localScale.x + changeSpeed, player.transform.localScale.y + changeSpeed, player.transform.localScale.z + changeSpeed);
            if (Mathf.Abs(player.transform.localScale.x - groß) <= 0.1f) Destroy(gameObject);
        }       
    }
}
